// impor modul momentjs
var moment = require("moment");
var salam = require("./salam"); // harus pake ./

// menggunakan module
console.log(salam.salamPagi());
// menggunakan modul momentjs
console.log("Sekarang: " + moment().format('D MMMM YYYY, h:mm:ss a'));
var fs = require('fs');

// membaca file menggunakan 
// https://attacomsian.com/blog/reading-writing-files-nodejs

// yang biasa
/* fs.readFile('mynewfile1.txt', (err, data) => {
    if(err) {
        throw err;
    }
    console.log(data.toString()); // toString bisa ilang gpp
});
 */
// yang sync
try {
    var data = fs.readFileSync('mynewfile1.txt');
    console.log(data.toString());
} catch (err) {
    console.error(err);
}
var http = require('http');
var fs = require('fs');

// usahakan di satu direktori

http.createServer(function (request, response) {

    // baca file
    fs.readFile('pempekk.PNG', (err, data));
    fs.readFile('index.html', (err, data) => {
        if (err) throw err;

        // kirim respon
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write(data);
        response.end();
    })
    
}).listen(8000);

console.log("Server running on /http://localhost:8000")
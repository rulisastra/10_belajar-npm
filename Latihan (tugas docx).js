// 1. hello world -- crtl + c -- untuk stop running server
console.log("Hello world");

// ada 2 cara buat jalanin server

// 1.1 request in server
var http = require('http');

var server = http.createServer(function (request,response){ // bisa disingkat
    res.end("Hi, Selamat datang di Node.Js");
});

server.listen(8000);

console.log("server running on http://localhost:8000");


// 1.2 Bikin server dengan modul HTTP
var http = require('http');
http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write('Hello <b>World</b>'); // bisa pake tag di write
    res.end();
}).listen(8000);

console.log("Server running on /http://localhost:8000") // titik dua koma ngga ngaruh


// 2.1 npm (Node Package Manager) 
// (1) kemudian bikin folder baru - bikin file index.js - 
// kemudian npm install <modules> - tambahin script di package.json
// kemudian tambahkan "server": "node index.js" di debug
// kemudian npm run server (perhatikan port nya!)

var http = require('http');
http.createServer(function (req, res) {

    res.end("Hello <b>World</b>"); // ngga bisa pake tag di end

}).listen(8000);

console.log("Server running on /http://localhost:8000")


// (2) kemudian bikin file module.js

// (3) kemudian bikin file salam.js

// (4) kemudian bikin file routing.js

// (5) kemudian bikin file query-string.js




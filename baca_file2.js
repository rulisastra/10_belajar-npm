// yang dikumpulkan
// panggil di cmd dengan node <namafile> tanpa spasi
// buka localhost di browser

var http = require('http');
var fs = require('fs');
var path = require('path');

http.createServer(function (request, response) {

    //setting path
    var filePath = '.' + request.url;
    if (filePath == './')
        filePath = 'index.html';
    var extname = path.extname(filePath);
    
    // menggabungkan jadi satu dg variabel // contentType
    var contentType = 'text/html';
    switch (extname) {
        case '.js':
            contentType = 'text/javascript';
            break;
        case '.css':
            contentType = 'text/css';
            break;
        case '.json':
            contentType = 'application/json';
            break;
        case '.png':
            contentType = 'image/png';
            break;      
        case '.jpg':
            contentType = 'image/jpg';
            break;
        case '.wav':
            contentType = 'audio/wav';
            break;
    }

    // baca file
    fs.readFile(filePath, (err, data) => {
        if (err) throw err;

        // kirim respon
        response.writeHead(200, {'Content-Type': contentType});
        response.write(data);
        response.end();
    })
    
}).listen(8000);

console.log("Server running on /http://localhost:8000")